# Makefile for building native Android app using Rust library:
# Run `make` from subdirectory containing this file due to relative paths.

# Android Studio is used here only for a conforming JRE and Kotlin compiler.

# For establishing Android API level, edit `app/build.gradle` file.

# Some Bash-specific shell escapes exist within commands, such as $((...))
# for computing max().

.PHONY: all
all: deps import release

# Where to download & compile things like Language Server Protocol helper:
BUILD_DEPS_PATH ?= ~/.cache/kotlin
# Where to install LSP:
LOCAL_BIN ?= /usr/local/bin

# This must match ./app/src/main/AndroidManifest.xml <manifest package="...">
PACKAGE ?= com.example.foo

# See Android Studio or SDK manager for highest numbered Available Package
# which should match `targetSdkVersion` in build.gradle file:
ANDROID_VERSION ?= $(shell \
	grep '^\s*targetSdkVersion' app/build.gradle | \
	sed 's/^[^0-9]*\([0-9]*\)[^0-9]*$$/\1/')

# Set NDK target API level to match `minSdkVersion` in build.gradle file:
NDK_VERSION ?= $(shell \
	grep '^\s*minSdkVersion' app/build.gradle | \
	sed 's/^[^0-9]*\([0-9]*\)[^0-9]*$$/\1/')

# If these paths change, any existing emulators configs will need to be
# updated; e.g., ~.android/avd/*.avd/hardware-qemu.ini, config.ini and
# snapshots/default_boot/hardware.ini
ANDROID_STUDIO ?= /home/android-studio
ANDROID_SDK ?= /home/android-sdk
ANDROID_NDK ?= /home/android-ndk

# Why two different executables with different sizes, Google?
## SDKMANAGER ?= ${ANDROID_SDK}/tools/bin/sdkmanager
SDKMANAGER ?= ${ANDROID_SDK}/cmdline-tools/latest/bin/sdkmanager

## EMULATOR ?= ${ANDROID_SDK}/tools/emulator
EMULATOR ?= ${ANDROID_SDK}/emulator/emulator

## AVDMANAGER ?= ${ANDROID_SDK}/emulator/emulator
AVDMANAGER ?= ${ANDROID_SDK}/cmdline-tools/latest/bin/avdmanager

JAVA_HOME ?= ${ANDROID_STUDIO}/jre
ARCH ?= $(shell arch)

APP_SDK_REQUIREMENTS_FILE ?= app-sdk-requirements.txt


# If developing for this project:
.PHONY: developer
developer: deps

.PHONY: deps
deps: linux-deps android-deps android-ndk-deps lsp update

# https://github.com/fwcd/kotlin-language-server/blob/master/BUILDING.md#building-1
LSP_PATH=server/build/install/server/bin
LSP_BIN=${BUILD_DEPS_PATH}/kotlin-language-server/${LSP_PATH}/kotlin-language-server
.PHONY: lsp
lsp: ${LSP_BIN} ${LOCAL_BIN}/kotlin-language-server

${LOCAL_BIN}/kotlin-language-server:
	[ -d ${LOCAL_BIN} ] || mkdir -p ${LOCAL_BIN}
	echo '#! /bin/sh' \
	  > ${LOCAL_BIN}/kotlin-language-server
	echo 'export JAVA_HOME=${JAVA_HOME}' \
	  >> ${LOCAL_BIN}/kotlin-language-server
	echo 'exec ${BUILD_DEPS_PATH}/kotlin-language-server/${LSP_PATH}/kotlin-language-server' \
	  >> ${LOCAL_BIN}/kotlin-language-server
	chmod a+x ${LOCAL_BIN}/kotlin-language-server

${LSP_BIN}: ${BUILD_DEPS_PATH}/kotlin-language-server
	(cd ${BUILD_DEPS_PATH}/kotlin-language-server && \
	  JAVA_HOME=${JAVA_HOME} ./gradlew :server:build)

${BUILD_DEPS_PATH}/kotlin-language-server:
	[ -d ${BUILD_DEPS_PATH} ] || mkdir -p ${BUILD_DEPS_PATH}
	git clone https://github.com/fwcd/kotlin-language-server.git \
	  ${BUILD_DEPS_PATH}/kotlin-language-server

# On Ubuntu 18.04 or OLDER, change `libvirt-daemon-system` to `libvirt-bin`
.PHONY: linux-deps
linux-deps:
	@echo "Invoking sudo for installing KVM and library dependencies"
	sudo apt-get install \
	  qemu-kvm libvirt-daemon-system ubuntu-vm-builder bridge-utils \
	  libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386

.PHONY: check-versions
check-versions:
	JAVA_HOME=${JAVA_HOME} \
	  ${SDKMANAGER} --list

.PHONY: android-deps
android-deps: android-deps-config update

.PHONY: android-deps-config
android-deps-config:
	[ -e    ${HOME}/.android/repositories.cfg ] || \
	  touch ${HOME}/.android/repositories.cfg
	@echo "Accept licenses:"
	JAVA_HOME=${JAVA_HOME} \
	  ${SDKMANAGER} --licenses

# Update all installed Android-Studio packages to their latest versions:
# https://developer.android.com/studio/command-line/sdkmanager.html
# As of 2020-03-25, `--package_file` only works for uninstall.  Thus,
# use `sed` to ensure proper shell quoting due to embedded semicolons.
# A fully populated entry in package_file would look like:
#  sources;android-29
#  platforms;android-29
#  system-images;android-29;google_apis_playstore;x86
#  system-images;android-29;google_apis;x86_64
#  system-images;android-29;google_apis;x86
#  system-images;android-29;default;x86
# Using system-images with "google" in the name is only necessary when
# using their APIs.  The "default" images are smaller-- less to
# download and less storage required.
# https://developer.android.com/studio/run/managing-avds#system-image
.PHONY: update
update:
	JAVA_HOME=${JAVA_HOME} \
	  ${SDKMANAGER} --install \
	    $(shell sed 's/^\s*\(\S*\)\s*$$/"\1"/' < ${APP_SDK_REQUIREMENTS_FILE})
	JAVA_HOME=${JAVA_HOME} \
	  ${SDKMANAGER} --update

# For hardware architectures and officially supported Android ABI list:
# https://developer.android.com/ndk/guides/abis#sa
# "Historically the NDK supported ARMv5 (armeabi), and 32-bit and
# 64-bit MIPS, but support for these ABIs was removed in NDK r17."
# 64-bit ARM and x86_64 began with NDK 21, so use Bash max() hack.
# May also be relevant:
# https://developer.android.com/ndk/guides/android_mk#target_arch_abi
# https://developer.android.com/ndk/guides/cmake#android_abi
.PHONY: android-ndk-deps
android-ndk-deps:
	@echo ''
	@echo 'Deprecation warnings regarding `toolchain/bin/clang++ src.cpp`'
	@echo 'do NOT apply to Rust, as `cargo` only uses `clang` as linker.'
	@echo ''
	${ANDROID_SDK}/ndk-bundle/build/tools/make_standalone_toolchain.py \
		--force --api $$((${NDK_VERSION} < 21 ? 21 : ${NDK_VERSION})) \
		--arch arm64 --install-dir ${ANDROID_NDK}/arm64
	${ANDROID_SDK}/ndk-bundle/build/tools/make_standalone_toolchain.py \
		--force --api ${NDK_VERSION} \
		--arch arm --install-dir ${ANDROID_NDK}/arm
	${ANDROID_SDK}/ndk-bundle/build/tools/make_standalone_toolchain.py \
		--force --api ${NDK_VERSION} \
		--arch x86 --install-dir ${ANDROID_NDK}/x86
	${ANDROID_SDK}/ndk-bundle/build/tools/make_standalone_toolchain.py \
		--force --api $$((${NDK_VERSION} < 21 ? 21 : ${NDK_VERSION})) \
		--arch x86_64 --install-dir ${ANDROID_NDK}/x86_64

SIBLING_REPO=../android-rust
JNI_LIBS=app/src/main/jniLibs
.PHONY: import
import:
	[ -f ${SIBLING_REPO}/target/aarch64-linux-android/release/librust.so ]
	[ -f ${SIBLING_REPO}/target/armv7-linux-androideabi/release/librust.so ]
	[ -f ${SIBLING_REPO}/target/i686-linux-android/release/librust.so ]
	[ -f ${SIBLING_REPO}/target/x86_64-linux-android/release/librust.so ]
	rm -rf ${JNI_LIBS}
	mkdir -p ${JNI_LIBS}
	mkdir ${JNI_LIBS}/arm64-v8a
	mkdir ${JNI_LIBS}/armeabi-v7a
	mkdir ${JNI_LIBS}/x86
	mkdir ${JNI_LIBS}/x86_64
	cp -p ${SIBLING_REPO}/target/aarch64-linux-android/release/librust.so \
	  ${JNI_LIBS}/arm64-v8a/
	cp -p ${SIBLING_REPO}/target/armv7-linux-androideabi/release/librust.so \
	  ${JNI_LIBS}/armeabi-v7a/
	cp -p ${SIBLING_REPO}/target/i686-linux-android/release/librust.so \
	  ${JNI_LIBS}/x86/
	cp -p ${SIBLING_REPO}/target/x86_64-linux-android/release/librust.so \
	  ${JNI_LIBS}/x86_64/

# This variant contains debugger symbols for LLDB -- NOT for release!
.PHONY: import-debug
import-debug:
	[ -f ${SIBLING_REPO}/target/aarch64-linux-android/debug/librust.so ]
	[ -f ${SIBLING_REPO}/target/armv7-linux-androideabi/debug/librust.so ]
	[ -f ${SIBLING_REPO}/target/i686-linux-android/debug/librust.so ]
	[ -f ${SIBLING_REPO}/target/x86_64-linux-android/debug/librust.so ]
	rm -rf ${JNI_LIBS}
	mkdir -p ${JNI_LIBS}
	mkdir ${JNI_LIBS}/arm64-v8a
	mkdir ${JNI_LIBS}/armeabi-v7a
	mkdir ${JNI_LIBS}/x86
	mkdir ${JNI_LIBS}/x86_64
	cp -p ${SIBLING_REPO}/target/aarch64-linux-android/debug/librust.so \
	  ${JNI_LIBS}/arm64-v8a/
	cp -p ${SIBLING_REPO}/target/armv7-linux-androideabi/debug/librust.so \
	  ${JNI_LIBS}/armeabi-v7a/
	cp -p ${SIBLING_REPO}/target/i686-linux-android/debug/librust.so \
	  ${JNI_LIBS}/x86/
	cp -p ${SIBLING_REPO}/target/x86_64-linux-android/debug/librust.so \
	  ${JNI_LIBS}/x86_64/

.PHONY: lint
lint:
	JAVA_HOME=${JAVA_HOME} \
	  ./gradlew --warning-mode all lint

# This is only for faster edit-compile-edit cycles; see also `build` target
.PHONY: compile
compile:
	JAVA_HOME=${JAVA_HOME} \
	  ./gradlew --warning-mode all compileDebugSources
	@[ ! -f app/build/outputs/apk/release/output.json -a \
	   ! -f app/build/outputs/apk/debug/output.json ] || \
	  (echo "Generated .apk files are *outdated*, so run: make build"; \
	   false)

.PHONY: build
build:
	JAVA_HOME=${JAVA_HOME} \
	  ./gradlew --warning-mode all -x lint app:build
	ls -lh app/build/outputs/apk/*

.PHONY: release
release:
	JAVA_HOME=${JAVA_HOME} \
	  ./gradlew --warning-mode app:build
	ls -lh app/build/outputs/apk/*

.PHONY: pre-submit
pre-submit:
	JAVA_HOME=${JAVA_HOME} \
	  ./gradlew signingReport

# This requires accepting their terms of service and then will PUBLISH
# to scans.gradle.com
.PHONY: publish-scan
publish-scan: build
	JAVA_HOME=${JAVA_HOME} \
	  ./gradlew --scan --warning-mode all app:build

# https://developer.android.com/studio/command-line/avdmanager.html
.PHONY: new-emulators
new-emulators:
	JAVA_HOME=${JAVA_HOME} \
	ANDROID_SDK=${ANDROID_SDK} \
	AVDMANAGER=${AVDMANAGER} \
	  ./new-emulators.py

# Use of `-no-audio` here keeps Emulator from interfering with music
# players on host/desktop system.
.PHONY: emulator
emulator:
	@[ "${DEVICE_NAME}" ] || \
	  (echo "Please set DEVICE_NAME env var to one of the following:"; \
	   ${EMULATOR} -list-avds; \
	   false)
	${EMULATOR} -avd "${DEVICE_NAME}" -no-snapshot-save -no-audio
# -change-language en.US -change-country US -change-locale en.US -timezone GMT-8

# Confirm with `app/src/main/AndroidManifest.xml` for `package=` value
# and `<activity name=".MainActivity">` for one that contains
# `<intent-filter><category>` with `LAUNCHER` or `MAIN`.
# https://developer.android.com/studio/command-line/adb
.PHONY: run
run:
	${ANDROID_SDK}/platform-tools/adb \
	  -e install -t -r app/build/outputs/apk/debug/app-debug.apk
	${ANDROID_SDK}/platform-tools/adb \
	  -e shell am start -n ${PACKAGE}/.MainActivity

.PHONY: run-release
run-release:
	${ANDROID_SDK}/platform-tools/adb \
	  -e install -t -r app/build/outputs/apk/release/app-release-unsigned.apk
	${ANDROID_SDK}/platform-tools/adb \
	  -e shell am start -n ${PACKAGE}/.MainActivity

# https://developer.android.com/guide/practices/verifying-apps-art
# https://android-developers.googleblog.com/2011/07/debugging-android-jni-with-checkjni.html
.PHONY: check-jni
check-jni: force-stop
	${ANDROID_SDK}/platform-tools/adb \
	  shell setprop debug.checkjni 1
	@echo "Confirm via logcat message: D Late-enabling CheckJNI"
	@echo "and check for messages beginning with: W JNI WARNING"
	@echo "This will be automatically disabled after device reboot."

.PHONY: check-jni-end
check-jni-end: force-stop
	${ANDROID_SDK}/platform-tools/adb \
	  shell setprop debug.checkjni 0

.PHONY: reload
reload: refresh
.PHONY: refresh
refresh: force-stop run

.PHONY: stop
stop: force-stop
.PHONY: kill
kill: force-stop
.PHONY: force-kill
force-kill: force-stop
.PHONY: force-stop
force-stop:
	${ANDROID_SDK}/platform-tools/adb \
	  shell am force-stop ${PACKAGE}

.PHONY: remove
remove: uninstall
.PHONY: uninstall
uninstall:
	${ANDROID_SDK}/platform-tools/adb \
	  shell pm uninstall ${PACKAGE}

.PHONY: clean-emulator
clean-emulator:
	for e in $(shell ${EMULATOR} -list-avds); do \
	  ${EMULATOR} -avd $$e -wipe-data; \
	  ${ANDROID_SDK}/platform-tools/adb \
	    -e shell pm uninstall ${PACKAGE}; \
	done

# Deploy to hardware device.
# If compiling one .apk to rule them all (architecture triples), use this:
.PHONY: deploy
deploy:
	${ANDROID_SDK}/platform-tools/adb \
	  -d install -t -r app/build/outputs/apk/debug/app-debug.apk
	${ANDROID_SDK}/platform-tools/adb \
	  -d shell am start -n ${PACKAGE}/.MainActivity

.PHONY: deploy-arm
deploy-arm:
	${ANDROID_SDK}/platform-tools/adb \
	  -d install -t -r app/build/outputs/apk/debug/app-armeabi-v7a-debug.apk
	${ANDROID_SDK}/platform-tools/adb \
	  -d shell am start -n ${PACKAGE}/.MainActivity

.PHONY: deploy-arm64
deploy-arm64:
	${ANDROID_SDK}/platform-tools/adb \
	  -d install -t -r app/build/outputs/apk/debug/app-arm64-v8a-debug.apk
	${ANDROID_SDK}/platform-tools/adb \
	  -d shell am start -n ${PACKAGE}/.MainActivity

# https://developer.android.com/studio/command-line/logcat
.PHONY: logcat
logcat: log
.PHONY: log
log:
	${ANDROID_SDK}/platform-tools/adb logcat

.PHONY: app-log
app-log:
	${ANDROID_SDK}/platform-tools/adb logcat -s astroxref

.PHONY: cpuinfo
cpuinfo:
	${ANDROID_SDK}/platform-tools/adb shell cat /proc/cpuinfo

FILENAME_ON_DEVICE="/sdcard/screen_$(shell date "+%Y-%m-%d-%H%M%s").png"
.PHONY: screenshot
screenshot:
	${ANDROID_SDK}/platform-tools/adb shell screencap -p ${FILENAME_ON_DEVICE}
	${ANDROID_SDK}/platform-tools/adb pull ${FILENAME_ON_DEVICE}
	${ANDROID_SDK}/platform-tools/adb shell rm ${FILENAME_ON_DEVICE}

# https://developer.android.com/studio/test/command-line
.PHONY: local-test
local-test:
	./gradle test

.PHONY: inst-test
inst-test: instrumented-test
.PHONY: instrumented-test
instrumented-test:
	./gradlew connectedAndroidTest

.PHONY: clean
clean:
	JAVA_HOME=${JAVA_HOME} ./gradlew clean

.PHONY: dist-clean
dist-clean: distclean
.PHONY: distclean
distclean: clean
	rm -rf ${JNI_LIBS}
	JAVA_HOME=${JAVA_HOME} ./gradlew cleanBuildCache

