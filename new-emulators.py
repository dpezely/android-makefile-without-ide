#! /usr/bin/env python3

# Create new Android virtual machines on host OS using Android SDK.

# This is purely optional and accommodates using the command-line
# instead of Android Studio.  It was one of the few tasks that their
# SDK (for API level 29) seemed incapable of handling without
# assistance, even after discovering which of two `avdmanager` to use.

# Some older Google devices have "skins" and others have profile data
# in devices.xml, while very few have both.

# See also:
# https://developer.android.com/studio/command-line/avdmanager.html

import os, csv, re, subprocess

ANDROID_SDK = os.getenv("ANDROID_SDK") or "/home/android-sdk"

AVDMANAGER = os.getenv("AVDMANAGER") or \
    ANDROID_SDK + "/cmdline-tools/latest/bin/avdmanager"

AVD_DIR = os.path.expanduser("~/.android/avd")

emulators = {}
with open('vm.csv', newline='') as csvfile:
    reader = csv.reader(csvfile)
    headers = reader.__next__()
    assert headers[:2] == ["Device name", "API level"], headers
    for row in reader:
        if row is None or len(row) < 2:
            continue
        device_name, api_level = row[:2]
        device_name = re.sub(r"[^-._ a-zA-Z0-9]", '', device_name)
        emulators[device_name] = api_level

os.putenv("TERM", "dumb")
for (device_name, api_level) in emulators.items():
    print(f"\nDevice name: {device_name}, API level: {api_level}")

    name = device_name.replace(' ', '_')
    skin = device_name.lower().replace(' ', '_')
    api = f"API_{api_level}"

    profile_exists = True
    cmd = ("grep", "-c", f"<d:name>{device_name}</",
           f"{ANDROID_SDK}/tools/lib/devices.xml")
    try:
        subprocess.run(args=cmd, check=True)
    except subprocess.CalledProcessError:
        profile_exists = False

    if profile_exists:
        avd_config_filename = f"{AVD_DIR}/{name}_{api}.avd/config.ini"
    else:
        name = f"{name}_{api}"
        avd_config_filename = f"{AVD_DIR}/{name}.avd/config.ini"

    if os.path.exists(avd_config_filename):
        print("\tAlready exists -- Nothing changed.")
        continue

    cmd = [AVDMANAGER, "create", "avd",
           "--name", name,
           "--package", f"system-images;android-{api_level};default;x86",
           "--abi", "default/x86"]
    if profile_exists: cmd.extend(["--device", device_name])
    # Bypass "create custom hardware profile? [no]" prompt
    with subprocess.Popen(args=cmd, stdin=subprocess.PIPE) as proc:
        os.write(proc.stdin.fileno(), b"no\n")

    if not os.path.exists(avd_config_filename):
        continue

    cmd = ("sed", "-i", ("s%" +
                         "skin.path=_no_skin" +
                         "%" +
                         "skin.dynamic=yes\\n" +
                         f"skin.name={skin}\\n" +
                         f"skin.path={ANDROID_SDK}/skins/{skin}" +
                         "%"),
           avd_config_filename)
    print(cmd)
    subprocess.run(args=cmd, check=True)
