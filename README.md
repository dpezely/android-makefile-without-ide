Android Makefile without IDE
============================

This is a [Makefile](https://www.gnu.org/software/make/manual/make.html)
for building native Android apps-- for people who prefer controlling
*everything* from the shell command-line.

By "everything", this means:

- Install and stay current by updating via `sdkmanager`
  + System images and other items are specified
    in [app-sdk-requirements.txt](./app-sdk-requirements.txt)
  + This text file is used for installation and updates, so some version
    numbers need to be revised from time to time.
- Import shared libraries compiled from another project; e.g., written in Rust
- Accommodate fast edit-compile-edit cycles versus full builds with `lint`
  or making a final release
- Create, start, stop, install .apk, uninstall .apk, wipe VMs
  via `avdmanager` and `emulator`
  + The *create* step is facilitated by
    a [Python script](./new-emulators.py) as better Bash
  + Edit list of devices to emulate in [vm.csv](./vm.csv)

See [TO DO](#to-do) for final few lingering items.

The [IDE](https://developer.android.com/studio/#downloads) is used here only
for a conforming JRE and [Kotlin compiler](https://kotlinlang.org/).

Otherwise,
[download just the SDK](https://developer.android.com/studio/#command-tools).

Your own Android project must supply the `gradlew` utility, along with any
other scaffolding that the IDE would generate for a new project.

This is "nearly" end-to-end because a licence must be accepted before
downloading the IDE or standalone SDK.  App signing and submitting to the
app store are not handled.

An example project using this is:
[native-android-kotlin-rust](https://gitlab.com/dpezely/native-android-kotlin-rust)
which is an anagram solver by the same author.

## Usage

**1.** Copy the Makefile file into your top-level Android project.
This location should already include the `gradlew` utility and `app/`
subdirectory containing `build.gradle` which specifies
`targetSdkVersion`, etc.

**2.** Edit the Makefile, and change `com.example.foo` to your package name.
This must match your `./app/src/main/AndroidManifest.xml` file contents.
Use your `<manifest package="...">` value.

**3.** Run `make` from subdirectory containing this file due to relative
paths.

Notes:

For establishing Android API level, keep those details within your
`app/build.gradle` file.  (A sample file is included.)

If using BSD Make or running a different login shell than Bash, some
Bash-specific shell escapes exist within commands and may need to change.

An example is `$((...))` for computing `max()`, which is necessary if
accommodating NDK versions earlier than API level 21, as that's the minimal
version allowed for 64-bit ARM and x86_64.  Otherwise, use simple variable
substitution syntax: `${FOO}`.  (Hint: API level 26 is Android 8.0 and as of
2020 is the oldest [version officially supported by
Google](https://support.google.com/googleplay/android-developer/answer/113469#targetsdk).)

## To Do

- Automate extraction of latest versions from `sdkmanager --list`
- Create new app scaffold files from command-line:
  + Try:
  [android-cheat](https://github.com/cirosantilli/android-cheat/blob/master/gradle.md)
  + See stackoverflow.com answers to:
  [How to create android project with gradle from command line?](https://stackoverflow.com/questions/20801042/how-to-create-android-project-with-gradle-from-command-line)
  + Or copy files from another project, such as
  [native-android-kotlin-rust](https://gitlab.com/dpezely/native-android-kotlin-rust)
  from which this one was spawned
- Handle code-signing and other preliminaries necessary for submitting to an
  app store
- Update for newer Android API level: the stable Android API level was
  29 (Android 10.0 Q) with a release candidate for 30 (R)

Contributors welcomed!

## Related Projects

Note: This is the **inverse** of using
[Android.mk](https://developer.android.com/ndk/guides/android_mk) and
[Application.mk](https://developer.android.com/ndk/guides/application_mk).
Those are for building a native library *from within the IDE*; whereas,
[this](./)  provides (nearly) end-to-end work-flow of developing an app
*without* it.

If you dislike Makefiles (or have difficulty thinking in terms of a command
*stack* or other principles on which it's based), consider this to be a
catalogue of shell commands.  Be at liberty to borrow/steal commands for
other build tools.

Similar tools & projects:

- <https://docs.bazel.build/versions/master/tutorial/android-app.html>
- <https://source.android.com/setup/build>
- others?

## Further Reading

May these help assist others new to Android NDK development:

SDK:

- <https://developer.android.com/studio/command-line/sdkmanager.html>
- <https://developer.android.com/studio/command-line/avdmanager.html>
- <https://developer.android.com/studio/command-line/adb>
- <https://developer.android.com/studio/command-line/logcat>
- <https://developer.android.com/studio/run/managing-avds#system-image>
- <https://developer.android.com/studio/test/command-line>

NDK:

- <https://developer.android.com/ndk/guides/abis#sa>
- <https://developer.android.com/ndk/guides/android_mk#target_arch_abi>
- <https://developer.android.com/ndk/guides/cmake#android_abi>
- <https://developer.android.com/guide/practices/verifying-apps-art>
- <https://android-developers.googleblog.com/2011/07/debugging-android-jni-with-checkjni.html>

JNI:

- <https://developer.android.com/training/articles/perf-jni#native-libraries>

LSP:

- <https://github.com/fwcd/kotlin-language-server/blob/master/BUILDING.md#building-1>

## License

MIT License

Essentially, do as you wish and without warranty from authors or maintainers.
